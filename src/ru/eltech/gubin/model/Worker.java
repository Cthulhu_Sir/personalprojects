package ru.eltech.gubin.model;

public class Worker {

    private boolean isBusy;

    public Worker(){
        isBusy = false;
    }

    public void makeBusy(){
        isBusy = true;
    }

    public void makeFree(){
        isBusy = false;
    }

    public boolean getState() {
        return !isBusy;
    }

}
